﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSelectionScript : MonoBehaviour
{
    public GameObject[] SelectablePlayers;
    public int playerSelectNumber;
    // Start is called before the first frame update
    void Start()
    {
        playerSelectNumber = 0;
        ActivatePlayer(playerSelectNumber);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ActivatePlayer(int x )
    {
        foreach(GameObject go in SelectablePlayers)
        {
            go.SetActive(false);
        }
        SelectablePlayers[x].SetActive(true);

        //Setting the player selection for the vehicle
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_SELECTION_NUMBER, playerSelectNumber} };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);
    }

    public void goToNextPlayer()
    {
        playerSelectNumber++;
        if(playerSelectNumber >= SelectablePlayers.Length)
        {
            playerSelectNumber = 0;
        }
        ActivatePlayer(playerSelectNumber);
    }

    public void goToPrevPlayer()
    {
        playerSelectNumber--;  
        if (playerSelectNumber < 0)
        {
            playerSelectNumber = SelectablePlayers.Length - 1;
        }
        ActivatePlayer(playerSelectNumber);
    }
}
