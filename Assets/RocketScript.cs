﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;

public class RocketScript : MonoBehaviour
{
    public float speed;

    public Rigidbody rb;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
      //  this.transform.rotation = Quaternion.Euler(new Vector3(90,0,0));
    }
    // Update is called once per frame
    private void FixedUpdate()
    {
      //  RocketMoving();
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(!collision.gameObject.GetComponent<PhotonView>().IsMine)
    //    {
    //        collision.gameObject.GetComponent<RaycastGun>().DamagePlayer(25);
            
    //    }
    //    Destroy(this.gameObject);
    //}
    void RocketMoving()
    {
        Vector3 direction = new Vector3(0, 0, 1);
        rb.velocity = direction * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<RaycastGun>())
        {
            other.gameObject.GetComponent<PhotonView>().RPC("DamagePlayer", RpcTarget.AllBuffered, 25f);

        }

        Destroy(gameObject);
    }
}
