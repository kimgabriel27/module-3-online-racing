﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{

    [SerializeField] WheelCollider frontRight;
    [SerializeField] WheelCollider frontLeft;
    [SerializeField] WheelCollider backRight;
    [SerializeField] WheelCollider backLeft;

    [SerializeField] Transform frontRightTransform;
    [SerializeField] Transform frontLeftTransform;
    [SerializeField] Transform backRightTransform;
    [SerializeField] Transform backLeftTransform;

    public float acceleration = 500f;
    public float brakingForce = 300f;
    public float maxTurnAngle = 15f;

    private float currentAcceleration = 0f;
    private float currentBrakeForce = 0f;
    private float currentTurnAngle = 0f;

    public bool isControlEnabled;
    private void Start()
    {
        isControlEnabled = false;
    }
    private void FixedUpdate()
    {
        if(isControlEnabled)
        {
            currentAcceleration = acceleration * Input.GetAxis("Vertical");


            if (Input.GetKey(KeyCode.Space))
            {
                currentBrakeForce = brakingForce * 2.0f;
            }
            else
            {
                currentBrakeForce = 0f;
            }

            //ApplyAcceleration to front wheels

            frontRight.motorTorque = currentAcceleration;
            frontLeft.motorTorque = currentAcceleration;
            backRight.motorTorque = currentAcceleration;
            backLeft.motorTorque = currentAcceleration;

            ///Applying brake to all wheels
            frontRight.brakeTorque = currentBrakeForce;
            frontLeft.brakeTorque = currentBrakeForce;
            backLeft.brakeTorque = currentBrakeForce;
            backRight.brakeTorque = currentBrakeForce;

            //Steering
            currentTurnAngle = maxTurnAngle * Input.GetAxis("Horizontal");
            frontLeft.steerAngle = currentTurnAngle;
            frontRight.steerAngle = currentTurnAngle;

            //Update WheelMeshes 
            UpdateWheel(frontLeft, frontLeftTransform);
            UpdateWheel(frontRight, frontRightTransform);
            UpdateWheel(backLeft, backLeftTransform);
            UpdateWheel(backRight, backRightTransform);
        }
       
    }

    void UpdateWheel(WheelCollider col, Transform trans)
    {

        //Get wheel col state
        Vector3 position;
        Quaternion rotation;
        col.GetWorldPose(out position, out rotation);

        trans.position = position;
        trans.rotation = rotation;
    }

}
