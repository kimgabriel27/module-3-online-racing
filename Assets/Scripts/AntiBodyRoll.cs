﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiBodyRoll : MonoBehaviour
{
    public WheelCollider WheelL;
    public WheelCollider WheelR;
    public float antiRoll = 5000.0f;    
    public float centerOfMass = -0.9f;
    private  Rigidbody Car;
    // Start is called before the first frame update
    void Start()
    {
        Car = GetComponent<Rigidbody>();
        Car.centerOfMass = new Vector3(0, centerOfMass, 0);
    }

    // Update is called once per frame
    void Update()
    {
        WheelHit hit;
        float travelL = 1.0f;
        float travelR = 1.0f;

        bool groundedL = WheelL.GetGroundHit(out hit);
        if (groundedL)
        {
            travelL = (-WheelL.transform.InverseTransformPoint(hit.point).y - WheelL.radius) / WheelL.suspensionDistance;
        }

        bool groundedR = WheelR.GetGroundHit(out hit);
        if (groundedR)
        {
            travelR = (-WheelR.transform.InverseTransformPoint(hit.point).y - WheelR.radius) / WheelR.suspensionDistance;
        }

        float antiRollForce = (travelL - travelR) * antiRoll;

        if (groundedL)
            Car.AddForceAtPosition(WheelL.transform.up * -antiRollForce, WheelL.transform.position);
        if (groundedR)
            Car.AddForceAtPosition(WheelR.transform.up * antiRollForce, WheelR.transform.position);


    }
}
