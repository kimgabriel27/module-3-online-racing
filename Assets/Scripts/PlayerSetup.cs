﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject playerUI;

    RaycastGun raycastGun;
    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
           //  GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<WheelController>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<RaycastGun>().enabled = false;
            camera.enabled = photonView.IsMine;
            playerUI.SetActive(false);
        
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
           /// GetComponent<VehicleMovementScript>().enabled = photonView.IsMine;
            GetComponent<WheelController>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = false;
            GetComponent<RaycastGun>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
            playerUI.SetActive(photonView.IsMine);
        
        }
    }

   
}
