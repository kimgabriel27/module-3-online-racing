﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

[RequireComponent(typeof(LineRenderer))]
public class RaycastGun : MonoBehaviourPunCallbacks
{
    [Header("Player Health")]
    public GameObject PlayerUI;
    public Image healthBar;
    public Text stateText;
    public float maxHealth = 100f;
    public float currHealth;


    [Header("Rocket Variables")]
    public GameObject rocketPrefab;
    public float bulletSpeed = 300;
    public float lifeTime = 3;
    public float rocketFireRate = 4f;

    [Header("Laser Variables")]
    public Camera playerCamera;
    public Transform laserOrigin;
    public float gunRange = 50f;
    public float fireRate = 4f;
    public float laserDuration = 0.05f;

    LineRenderer laserLine;
    float fireTimer;
    float rocketTimer;

    int deathCount = 0;
    public bool isDead = false;
    public enum RaiseEventCode
    {
        DeathOfPlayerEventCode = 0,
        CallPlayerWhoIsDead = 1
    }
    void Awake()
    {
        laserLine = GetComponent<LineRenderer>();
        
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnDieEvent;
        PhotonNetwork.NetworkingClient.EventReceived += OnCallDeadPlayers;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnDieEvent;
        PhotonNetwork.NetworkingClient.EventReceived -= OnCallDeadPlayers;
    }
    private void Start()
    {
        currHealth = maxHealth;
        healthBar.fillAmount = currHealth / maxHealth;
        stateText = PlayerUI.transform.GetChild(2).GetComponent<Text>();
        stateText.text = "";
    }

    void Update()
    {
        fireTimer += Time.deltaTime;
        rocketTimer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.RightControl) && fireTimer > fireRate)
        {
            fireTimer = 0;
            photonView.RPC("FireLaser", RpcTarget.All);
        }

        if (Input.GetKeyDown(KeyCode.RightShift) && rocketTimer > rocketFireRate)
        {
            rocketTimer = 0;
            photonView.RPC("FireRocket", RpcTarget.All);

        }
    }

    IEnumerator ShootLaser()
    {
        laserLine.enabled = true;
        yield return new WaitForSeconds(laserDuration);
        laserLine.enabled = false;
    }

    IEnumerator DestroyRocketAfterTime(GameObject rocket, float delay)
    {
        yield return new WaitForSeconds(delay);
        Destroy(rocket);
    }


    [PunRPC]
    public void DamagePlayer(float damage)
    {
        
        currHealth -= damage;
        healthBar.fillAmount = currHealth / maxHealth;

        if(currHealth <= 0)
        {
            Debug.Log("Damaged");
            photonView.RPC("Die", RpcTarget.AllBuffered);
        }
    }

   
    void OnDieEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.DeathOfPlayerEventCode)
        {
           
            Debug.Log("Dead " + photonView.name);

            GetComponent<WheelController>().isControlEnabled = false;

            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfDeadPlayer = (string)data[0];
            deathCount = (int)data[1];

            Debug.Log(nickNameOfDeadPlayer + " " + deathCount);

            //Check if one playerAlive
            if(RacingGameManager.instance.playersAlive == 1)
            {
                foreach (GameObject p in RacingGameManager.instance.playersInGame)
                {
                    if (!p.GetComponent<RaycastGun>().isDead && p == this.gameObject)
                    {
                        RacingGameManager.instance.nameOfWinner = p.GetComponent<PhotonView>().Owner.NickName;
                        photonView.RPC("ShowWinner", RpcTarget.AllBuffered, p.GetComponent<PhotonView>().name);


                        stateText.text = "You are the last man Standing. You Win!";
                        stateText.color = Color.green;

                        object[] otherData = new object[] { p.GetComponent<PhotonView>().name};
                        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
                        {
                            Receivers = ReceiverGroup.All,
                            CachingOption = EventCaching.AddToRoomCache

                        };
                        SendOptions sendOptions = new SendOptions
                        {
                            Reliability = false
                        };

                        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.CallPlayerWhoIsDead, otherData, raiseEventOptions, sendOptions);
                    }

                


                    //photonView.RPC("ShowWinner", RpcTarget.AllBuffered, p.GetComponent<PhotonView>().name);
                }

          

            }
         
        }
    }

    void OnCallDeadPlayers(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.CallPlayerWhoIsDead)
        {
            object[] data = (object[])photonEvent.CustomData;
            string nameWinner = (string)data[0];
            foreach (GameObject p in RacingGameManager.instance.playersInGame)
            {
                if(p.GetComponent<RaycastGun>().isDead)
                {
                    stateText.text = "The Last man Standing is " + nameWinner + ". You Lose!";
                    
                   // photonView.RPC("ShowWinner", RpcTarget.AllBuffered, nameWinner);
                }
            }
        }
    }
    

        
    [PunRPC]
    void Die()
    {
        if (photonView.IsMine && currHealth <= 0)
        {
            deathCount++;
            RacingGameManager.instance.playersAlive--;
            isDead = true;
            string name = photonView.name;

            object[] data = new object [] {name, deathCount };
     
            Debug.Log("Dead " + photonView.name);
            stateText.text = "You are Eliminated";
            GetComponent<WheelController>().isControlEnabled = false;

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache

            };
            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)RaiseEventCode.DeathOfPlayerEventCode, data, raiseEventOptions, sendOptions);
        }

    }

    [PunRPC]
    void FireLaser()
    {
        laserLine.SetPosition(0, laserOrigin.position);
        Vector3 rayOrigin = playerCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast(rayOrigin, playerCamera.transform.forward, out hit, gunRange))
        {
            Debug.Log(hit.transform.gameObject);
            laserLine.SetPosition(1, hit.point);
            if (!hit.transform.gameObject.GetComponent<PhotonView>().IsMine && hit.transform.gameObject.GetComponent<RaycastGun>())
            {
                hit.transform.gameObject.GetComponent<PhotonView>().RPC("DamagePlayer", RpcTarget.AllBuffered, 30f);
            }

        }
        else
        {
            laserLine.SetPosition(1, rayOrigin + (playerCamera.transform.forward * gunRange));
        }
        StartCoroutine(ShootLaser());
    }

    [PunRPC]
    void FireRocket()
    {

        GameObject rocket = Instantiate(rocketPrefab);
        rocket.transform.position = laserOrigin.transform.position;
        Physics.IgnoreCollision(rocket.GetComponent<Collider>(), GetComponent<Collider>());

     
        Vector3 rotation = rocket.transform.rotation.eulerAngles;

        rocket.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);

        rocket.GetComponent<Rigidbody>().AddForce(laserOrigin.forward * bulletSpeed, ForceMode.Impulse);

        StartCoroutine(DestroyRocketAfterTime(rocket, lifeTime));

    }
    
    [PunRPC]
    void ShowWinner(string nickName)
    {
        if(photonView.name != nickName)
        {
            stateText.text = "The Last man Standing is " + nickName + ". You Lose!";
        }
        else
        {
            stateText.text = "You are the last man Standing. You Win!"; 
        }

        StartCoroutine(BackToLobby());
    }

    IEnumerator BackToLobby()
    {
        yield return new WaitForSeconds(4);

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            if (p.NickName == this.photonView.name && p.IsMasterClient == PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.AutomaticallySyncScene = true;
                PhotonNetwork.LoadLevel("LobbyScene");
            }
        }
     

    }
    
}
