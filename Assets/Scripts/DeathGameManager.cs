﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class DeathGameManager : MonoBehaviourPunCallbacks
{

    public static DeathGameManager instance = null;
    public List<PhotonView> playersAlive = new List<PhotonView>();

    private int deathCount = 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

   
    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code ==(byte)RaiseEventCode.DeathOfPlayerEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfDeadPlayer = (string)data[0];
            deathCount = (int)data[1];

            Debug.Log(nickNameOfDeadPlayer + " " + deathCount);

           
            
        }
    }

    public enum RaiseEventCode
    {
        DeathOfPlayerEventCode = 0
    }
    
    void PlayerDeath()
    {
        deathCount++;
        string nickName = photonView.Owner.NickName;
        object[] data = new object[] {nickName , deathCount};


        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
            
        };
        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.DeathOfPlayerEventCode, data, raiseEventOptions, sendOptions);
    }
}
