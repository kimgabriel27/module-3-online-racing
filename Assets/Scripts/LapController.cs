﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0
    }

    private int finishOrder = 0;

    //Add listeners
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }


    void OnEvent (EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfFinishedPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2]; 


            Debug.Log(nickNameOfFinishedPlayer + " " + finishOrder);

            GameObject orderUIText = RacingGameManager.instance.finisherTextUI[finishOrder - 1];
            orderUIText.SetActive(true);

            if(viewId == photonView.ViewID)//this is you
            {
                orderUIText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishedPlayer + "(YOU)";
                orderUIText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUIText.GetComponent<Text>().text = finishOrder + " " + nickNameOfFinishedPlayer ;
            }


           
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if(RacingGameManager.instance.lapTriggers != null)
        {
            foreach (GameObject go in RacingGameManager.instance.lapTriggers)
            {
                lapTriggers.Add(go);
            }

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(lapTriggers.Contains(other.gameObject))
        {
            int indexOfTrigger = lapTriggers.IndexOf(other.gameObject);
            lapTriggers[indexOfTrigger].SetActive(false);
        }

        if(other.gameObject.tag == "FinishTrigger")
        {
            GameFinish();
        }
    }

    public void GameFinish()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovementScript>().enabled = false;

        finishOrder++;
        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        //event date
        object[] data = new object[] { nickName, finishOrder, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };
        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };


        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
    }
}
